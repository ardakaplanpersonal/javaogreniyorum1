package com.example.ardak.myapplication;

/**
 * Created by ardak on 5.11.2017.
 */

public class _3_MatematikselIslemler {

    public static void main(String[] args) {

        int sayi1 = 25;
        int sayi2 = 4;

        int toplam = sayi1 + sayi2;
        int cikarmaSonucu = sayi1 - sayi2;
        int carpmaSonucu = sayi1 * sayi2;
        float bolumSonucu = (float) sayi1 / sayi2;


        // System.out.println("toplam = " + toplam);


        //System.out.println(  "3 + 4 = " + toplam);

        System.out.println(sayi1 + " + " + sayi2 + " = " + toplam);
        System.out.println(sayi1 + " + " + sayi2 + " = " + (sayi1 + sayi2));

        System.out.println();

        System.out.println(sayi1 + " - " + sayi2 + " = " + cikarmaSonucu);
        System.out.println(sayi1 + " - " + sayi2 + " = " + (sayi1 - sayi2));

        System.out.println();

        System.out.println(sayi1 + " * " + sayi2 + " = " + carpmaSonucu);
        System.out.println(sayi1 + " * " + sayi2 + " = " + (sayi1 * sayi2));

        System.out.println();

//        System.out.println(sayi1 + " / " + sayi2 + " = " + (sayi1 / sayi2));//hatalı
        System.out.println(sayi1 + " / " + sayi2 + " = " + bolumSonucu);
        System.out.println(sayi1 + " / " + sayi2 + " = " + ((float) sayi1 / sayi2));


        System.out.println();

//        int t;
//        System.out.println("sayi 3 set edilmeden hali = " + t);

        float sayi3;
        sayi3 = 30.4f;

        float sayi4 = 5.3f;

        float bolmeSonucu = sayi3 / sayi4;


        System.out.println(sayi3 + " / " + sayi4 + " = " + bolmeSonucu);
        System.out.println(sayi3 + " / " + sayi4 + " = " + (sayi3 / sayi4));

    }
}
