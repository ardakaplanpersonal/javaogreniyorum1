package com.example.ardak.myapplication;

/**
 * Created by Arda Kaplan
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class _9_WhileDongusu {

    public static void main(String[] args) {

        int limit = 10;
        int sayi = 0;

        while (sayi < limit) {

            System.out.println(sayi);

            sayi++;
        }
    }
}
