package com.example.ardak.myapplication.baska.siniflarla.calisma;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Metotlar {


    public static void merhabaDe() {

        int sayi = 9;

        System.out.println("MERHABA!!");
    }

    public static void çarp(int sayi1, int sayi2) {

        System.out.println("çarpım sonucu = " + (sayi1 * sayi2));
    }

    public static int degeriAl(int sayi1) {

        return sayi1 * 5;

    }
}
