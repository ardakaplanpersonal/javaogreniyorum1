package com.example.ardak.myapplication;

/**
 * Created by Arda Kaplan
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class _14_Metotlar3 {

    public static void main(String[] args) {

        System.out.println(kelimeyiArayaKoy("ağaç"));
    }

    public static String kelimeyiArayaKoy(String arayaKoyulacakMetin) {

//        String metin1 = "Araya koyulacak metin, ";
//        String metin2 = ", başarıyla koyuldu.";
//
//        String sonuc = metin1 + arayaKoyulacakMetin + metin2;
//
//        return sonuc;


        return "Araya koyulacak metin, " + arayaKoyulacakMetin + ", başarıyla koyuldu.";
    }
}
