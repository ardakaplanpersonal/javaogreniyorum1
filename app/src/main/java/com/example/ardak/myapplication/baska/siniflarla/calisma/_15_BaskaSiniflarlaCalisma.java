package com.example.ardak.myapplication.baska.siniflarla.calisma;


import com.example.ardak.myapplication.yenipaket.denemepaket.DenemeSinif;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class _15_BaskaSiniflarlaCalisma {

    public static void main(String[] args) {

        Metotlar.merhabaDe();

        Metotlar.çarp(3, 4);

        System.out.println(Metotlar.degeriAl(10));

        ///////////////

        System.out.println(Degiskenler.genelString);

        System.out.println(Degiskenler.genelDegisken);

        System.out.println(com.example.ardak.myapplication.yenipaket.denemepaket.Degiskenler.degisken);

        System.out.println(DenemeSinif.deneme);

    }
}
