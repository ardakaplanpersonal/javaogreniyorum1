package com.example.ardak.myapplication;

/**
 * Created by Arda Kaplan on 6.11.2017.
 * <p>
 * arda.kaplan09@gmail.com
 * www.ardakaplan.com
 */

public class _5_ForDongusu {

    public static void main(String[] args) {

//        int sayi1 = 0;
//
//        System.out.println(sayi1);
//
//        // sayi1 = sayi1 + 1;
//        sayi1++;
//
//        System.out.println(sayi1);
//
//        sayi1--;
//
//        System.out.println(sayi1);

//        for (int i = 0; i < 5; i = i + 2) {
//
//            System.out.println("java Öğreniyorum " + i);
//        }

        String[] dizi = {"ahmet", "ayşe", "ayça", "fatma", "arda"};

        System.out.println("1. göz değeri : " + dizi[0]);
        System.out.println("2. göz değeri : " + dizi[1]);
        System.out.println("3. göz değeri : " + dizi[2]);
        System.out.println("4. göz değeri : " + dizi[3]);

        System.out.println("////////////////////////////////////");

        System.out.println("Dizinin uzunluğu = " + dizi.length);

        for (int i = 0; i < dizi.length; i++) {

            System.out.println((i + 1) + ". göz değeri :" + dizi[i]);
        }

    }
}
