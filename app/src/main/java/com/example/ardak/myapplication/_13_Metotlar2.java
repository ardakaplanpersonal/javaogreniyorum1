package com.example.ardak.myapplication;

/**
 * Created by Arda Kaplan
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class _13_Metotlar2 {

    public static void main(String[] args) {

        int geriGelenDeger = topla(1, 2);

        System.out.println("1 ve 2 nin toplamı : " + geriGelenDeger);

        System.out.println(topla(4, 56));

    }

    public static int topla(int sayi1, int sayi2) {

        System.out.println("Topla metodu çağrıldı.");

        return sayi1 + sayi2;
    }
}
