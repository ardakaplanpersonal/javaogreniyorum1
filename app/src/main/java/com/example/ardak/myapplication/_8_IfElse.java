package com.example.ardak.myapplication;

/**
 * Created by Arda Kaplan
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class _8_IfElse {
    public static void main(String[] args) {

        /*

        if(){

        }else if(){

        }else if(){

        }else if(){

        }else if(){

        }
        else{

        }

         */


        int sayi = 51;

        if (sayi < 10) {
            System.out.println("Girilen sayı 10 dan küçüktür");
        } else if (sayi > 10) {
            System.out.println("Girilen sayı 10 dan büyüktür");
        } else {
            System.out.println("Girilen sayı 10 a eşittir");
        }


        boolean booleanDegisken = false;

        if (booleanDegisken) {
            System.out.println("Degisken true");
        } else {
            System.out.println("Degisken false");
        }

        System.out.println();

        boolean[] dizi = {true, false};

        for (int i = 0; i < 2; i++) {

            for (int j = 0; j < 2; j++) {

                System.out.println(dizi[i] + " || " + dizi[j] + " = " + (dizi[i] || dizi[j]));
            }
        }

        System.out.println();

        for (int i = 0; i < 2; i++) {

            for (int j = 0; j < 2; j++) {

                System.out.println(dizi[i] + " && " + dizi[j] + " = " + (dizi[i] && dizi[j]));
            }
        }


    }
}
