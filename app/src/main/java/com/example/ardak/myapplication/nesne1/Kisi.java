package com.example.ardak.myapplication.nesne1;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Kisi {

    public long kimlikNumarasi;
    public String ismi;
    public String soyismi;

    public static int sayi = 0;

    public void yuru(int yurunecekMetre) {

        System.out.println(ismi + " " + yurunecekMetre + " metre yürüdü.");
    }

    public static void yuru2(int yurunecekMetre) {

      //  System.out.println(ismi + " " + yurunecekMetre + " metre yürüdü.");
    }
}
