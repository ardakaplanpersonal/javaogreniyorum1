package com.example.ardak.myapplication.modifiers;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Metotlar {

    public static String publicDegisken = "AAA";

    private static String privateDegisken = "BBB";

    protected static String protectedDegisken = "CCC";

    static String Degisken = "DDD";

    public static void ekranaYazdir() {

        System.out.println("BEN ÇAĞRILDIM");
    }

    static void deneme() {

    }
}
