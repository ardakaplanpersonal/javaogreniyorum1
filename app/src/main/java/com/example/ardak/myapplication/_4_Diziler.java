package com.example.ardak.myapplication;

/**
 * Created by ardak on 5.11.2017.
 */

public class _4_Diziler {

    public static void main(String[] args) {

        int[] tamSayiDizisi = {1, 2, 3, 4, 5, 6, 7, 8};
        long longDizisi[] = {231231L, 123123L};
        String stringDizisi[] = {"arda", "kaplan", "mehmet"};

        System.out.println("tam sayi dizisinin 3. elemanı = " + tamSayiDizisi[2]);

        System.out.println("long dizisinin 2. elemanı = " + longDizisi[1]);

        System.out.println("string dizisinin 3. elemanı = " + stringDizisi[2]);

        int dizi[] = new int[3];

        System.out.println(" 3. göz " + dizi[2]);

        dizi[2] = 4;


        System.out.println(" 1. göz " + dizi[0]);
        System.out.println(" 2. göz " + dizi[1]);
        System.out.println(" 3. göz " + dizi[2]);


        String[] metinDizisi = new String[2];


        metinDizisi[1] = "araba";

        System.out.println(metinDizisi[0]);
        System.out.println(metinDizisi[1]);


        String isim = " ";

        System.out.println("-" + isim + "-");

        isim = "arda";

        System.out.println("-" + isim + "-");

        ////////////////////////////

        String isim2;

        isim2 = "arda";

        System.out.println("isim2 :" + isim2);

        int y;

        y = 4;

        System.out.println(y);

    }
}
