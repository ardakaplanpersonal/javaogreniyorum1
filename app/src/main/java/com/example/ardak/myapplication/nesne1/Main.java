package com.example.ardak.myapplication.nesne1;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        Kisi kisi1 = new Kisi();

        kisi1.kimlikNumarasi = 11111111111L;
        kisi1.ismi = "Arda";
        kisi1.soyismi = "Kaplan";


        Kisi kisi2 = new Kisi();

        kisi2.ismi = "Ahmet";
        kisi2.soyismi = "Can";
        kisi2.kimlikNumarasi = 123;


        kisi1.yuru(25);

        kisi2.yuru(5);

        System.out.println(Kisi.sayi);

        kisi2.sayi++;

        System.out.println(kisi1.sayi);


    }
}
