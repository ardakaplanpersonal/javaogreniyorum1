package com.example.ardak.myapplication;

/**
 * Created by Arda Kaplan
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class _7_Operatorler {
    public static void main(String[] args) {


        /*

        küçüktür "<"
        büyüktür ">"

        küçük eşit "<="
        büyük eşit ">="

        eşit mi "=="
        eşit değil "!="

        değil "!"


        ve/and işlemi "&&"

        veya/or işlemi "||"


         */


        int sayi1 = 2;
        int sayi2 = 6;


        System.out.println("1- " + (sayi1 < sayi2));
        System.out.println("2- " + (sayi1 > sayi2));
        System.out.println("3- " + (sayi1 <= sayi2));
        System.out.println("4- " + (sayi1 >= sayi2));
        System.out.println("5- " + (sayi1 <= sayi2));
        System.out.println("6- " + (sayi1 == sayi2));
        System.out.println("7- " + (sayi1 != sayi2));

        boolean booleanTrue = true;
        boolean booleanFalse = false;

        System.out.println("8- " + (booleanFalse && booleanTrue));
        System.out.println("9- " + (booleanTrue && booleanTrue));

        System.out.println("10- " + (booleanFalse || booleanFalse));
        System.out.println("11- " + (booleanFalse || booleanTrue));
    }
}
