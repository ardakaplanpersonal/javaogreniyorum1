package com.example.ardak.myapplication;

/**
 * Created by Arda Kaplan
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class _12_Metotlar {

    static int sayiGenel = 5;

    public static void main(String[] args) {

        int sayi1 = 0;

        sayiGenel++;

        System.out.println("Uygulamam başladı." + sayiGenel);


        deneme();

        ekranaYazdir("Ekrana yazdir metodunu çalıştırdım.");


        sayilariTopla(1, 3);
    }

    public static void deneme() {

        int sayi1 = 0;

        sayiGenel++;

        System.out.println("Deneme metodu çağrıldı." + sayiGenel);
    }

    public static void ekranaYazdir(String metin) {

        int sayi1 = 0;

        System.out.println(metin);
    }

    public static void sayilariTopla(int sayi1, int sayi2) {

        System.out.println(sayi1 + sayi2);
    }


}
