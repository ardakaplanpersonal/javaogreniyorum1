package com.example.ardak.myapplication;

/**
 * Created by ardak on 3.11.2017.
 */

public class _2_TemelVeriTipleri {

    public static void main(String[] args) {

//        tam sayilari tutmak icin
//        byte,
//        short,
//        int,
//        long,

//        kesirli sayıları tutmak için
//        float,
//        double,

//        doğru yanlış tutmak için
//        boolean,

//        karakter
//        char

        System.out.println("*****************TAM SAYILAR*****************");

        byte byteDegiskeni = 127;
        System.out.println("byteDegiskeni = " + byteDegiskeni);

        short shortDegiskeni = 56;
        System.out.println("shortDegiskeni = " + shortDegiskeni);

        int intDegiskeni = 1465786;
        System.out.println("intDegiskeni = " + intDegiskeni);

        long longDegiskeni = 922337203685477580L;
        System.out.println("longDegiskeni = " + longDegiskeni);

        System.out.println("*****************KESİRLİ SAYILAR*****************");

        float floatDegiskeni = 35.5f;
        System.out.println("floatDegiskeni = " + floatDegiskeni);

        double doubleDegiskeni = 578966.458787;
        System.out.println("doubleDegiskeni = " + doubleDegiskeni);

        System.out.println("*****************DOĞRU YANLIŞ*****************");

        boolean booleanDegiskeni = true;
        System.out.println("booleanDegiskeni = " + booleanDegiskeni);

        booleanDegiskeni = false;
        System.out.println("booleanDegiskeni = " + booleanDegiskeni);

        System.out.println("*****************KARAKTER*****************");
        char charDegiskeni = 66;
        char charDeger1 = 65;
        char charDeger2 = 'A';

        System.out.println("charDegiskeni = " + charDegiskeni);
        System.out.println("charDeger1 = " + charDeger1);
        System.out.println("charDeger2 = " + charDeger2);

        //https://www.mobilhanem.com/temel-java-dersleri-java-veri-tipleri-degiskenler/
    }
}
