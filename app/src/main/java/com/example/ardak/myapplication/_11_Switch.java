package com.example.ardak.myapplication;

/**
 * Created by Arda Kaplan
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class _11_Switch {

    public static void main(String[] args) {

        int ayNumarasi = 2;

        String ayIsmi;

//        if (ayNumarasi == 1) {
//
//            ayIsmi = "Ocak";
//
//        } else if (ayNumarasi == 2) {
//
//            ayIsmi = "Şubat";
//
//        } else if (ayNumarasi == 3) {
//
//            ayIsmi = "Mart";
//
//        } else {
//
//            ayIsmi = "Geçersiz ay";
//        }

//        System.out.println(ayIsmi);

        System.out.println();
        System.out.println();

        switch (ayNumarasi) {

            case 1:
                ayIsmi = "Ocak";
                break;

            case 2:
                ayIsmi = "Şubat";
                break;

            case 3:
                ayIsmi = "Mart";
                break;

            default:
                ayIsmi = "Geçersiz ay";

        }

        System.out.println(ayIsmi);
    }
}
