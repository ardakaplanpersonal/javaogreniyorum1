package com.example.ardak.myapplication;

/**
 * Created by Arda Kaplan
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class _10_DoWhileDongusu {

    public static void main(String[] args) {

        int limit = 10;
        int sayi = 0;

        do {

            System.out.println(sayi);

            sayi++;

        } while (sayi < limit);
    }
}
