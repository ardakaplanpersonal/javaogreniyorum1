package com.example.ardak.myapplication;

/**
 * Created by Arda Kaplan on 6.11.2017.
 * <p>
 * arda.kaplan09@gmail.com
 * www.ardakaplan.com
 */

public class _6_ForDongusu2 {
    public static void main(String[] args) {


//        System.out.println("*****");
//        System.out.println("*");
//        System.out.println("**");
//        System.out.println("***");
//        System.out.println("****");

        int kareUzunlugu = 5;

        for (int i = 0; i < kareUzunlugu; i++) {

            for (int j = 0; j < i; j++) {

                System.out.print("*");
            }

            System.out.println();
        }

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();

        //continue

        for (int i = 0; i < 10; i++) {

            for (int j = 0; j < i; j++) {

                if (i % 2 == 0) {
                    continue;
                }

                System.out.print(i);
            }

            System.out.println();
        }

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();

        //break

        int limit = 16;

        for (int i = 0; i < limit; i++) {

            if (i == 12) {
                break;
            }

            System.out.print(i);
        }


    }
}
